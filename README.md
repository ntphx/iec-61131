# IEC 61131

## Introduction

IEC (International Electrotechnical Commission) 61131 consists of 7 parts
which consist of 62 feature tables that define the standard. Manufacturer
product compliance is obtained by documenting (un)availability of these features
in a product.

This document will mainly focus on IEC 61131-3: "PLC Programming Languages".

All 61131 compliant PLC systems share a common base by way of understanding
concepts such as Configurations, Resources, POU's, Task Management, and more...
The programming of these systems may be done using a range of languages.

This document will mainly deal with the Structured Text language.

|Graphical|Text-Based|
|---      |---       |
|Ladder Diagram|Instruction List|
|Function Block Diagram|Structured Text|

## Basic Concepts

![configuration](images/configuration.png)
 
![example_config](images/example_config.png)

### Configuration

In complex control systems, such as a safety system for a factory, multiple
physical PLC devices may be required to work together while located in different
positions across the plant.

The configuration contains all information and instructions required to
coordinate such a system. In software terms it may be considered as the global
scope or superclass.

### Resource

Resources are processors available to a configuration to do work on. Usually a
physical PLC will have one CPU meaning the resource is considered the CPU.
However some PLC's may have multiple CPU's or special co-processors. In that
case each processor is their own resource.

### Tasks

The PLC (resource) is given one or more tasks. A task usually consists of
running POU's. The way tasks behave depends on the run mode of the PLC.

#### Cyclical

Each scan+process cycle is of the same pre-determined length. For example with a
cycle of duration `T#200ms` there would be 5 cycles every second. If code has
not completed before the end of the cycle it is interrupted.

#### Freewheeling

The PLC waits for all tasks to complete before starting a new cycle

#### Event Controlled

Task execution starts when triggered by a condition or event. For example a
safety stop systems only starts when heat is detected.

### Priority

Where multiple tasks must run in concurrence there is potential for conflict
whereby these tasks may demand CPU execution at the same time.

To avoid conflict we can assign priority levels to tasks. Levels range from `1`
to `15` with the lower number being the higher priority.

Conflict resolution between competing tasks occurs (implementation dependant) by
either:

1. The lower priority program is immediately interrupt and the high priority
program is executed.
2. Or the lower priority program may finish after which the high priority
program is executed.

### Watchdog Timers

Due to the cyclical nature of PLC operation safeguards are put in place to
prevent the PLC blocking on execution for too long. This may happen with an infinite loop.
When a predetermined time is exceeded and the PLC is not ready for the next scan
cycle the watchdog timer may put the PLC into an error state. This prevents any
furher program execution.

## Program Organizational Unit (POU)

POU's are the basic code blocks for any PLC project. To simplify
historical complexity the standard defines just four POU types. This document
will focus on the first three. (Classes being the fourth)

![old_din_new_pou](images/old_new_pou.png)

A POU always starts with the Declaration part followed by the Implementation
part.

Declaration is always done in Structured Text. In the declaration part all
required variables must be declared to allow for the compiler to assign
sufficient memory to run the POU.

The code implementation of the POU may be done using either textual or graphical
programming languages available.

![pou_diagram](images/pou_diagram.png)


Calls from one POU to another are allowed but restricted to the following
diagram. Recursive calling of a POU is **illegal** under the standard. PLC systems
operate with limited resources and the total memory footprint of recursive
programs cannot be determined beforehand.

TODO insert POU calling diagram

A summary description of the different POU's follows here.
For in-depth review see their respective sections.

### PROGRAM

A PROGRAM is instantiated before use. The PROGRAM instance is then assigned to a
task within the configuration. Several instances of the same PROGRAM may run
concurrently.

A PROGRAM POU may declare variables using the direct memory access method
allowing access to physical I/O data.

### FUNCTION_BLOCK

The FUNCTION_BLOCK must be instantiated before use. The instance reserves memory
for its variables. This means repeated calls to the same FB instance with the
same format parameters may yield different results.

A FB does not directly return a value. Instead the output data is stored in
special variables per instance and is accessed in the familiar <object>.<field> method.

Since every FB instance reserves memory in the stack care must be taken when
designing a FUNCTION_BLOCK POU. Redundant copies of data must be avoided and the
use of temporary variables or variables storing pointers is recommended.

A FUNCTION_BLOCK POU may not declare variables using the direct memory access
method. The FUNCTION_BLOCK definition is available globally but FB instances are
local.

### FUNCTION

The FUNCTION POU can take an arbitrary number of inputs but can only return a
single return value. The FUNCTION is not instantiated and thus has no memory of
its own. This means that repeated calls to the same function with the same
formal parameters always yields the same result.

Any FUNCTION is globally scoped and may be called in any other POU within the
configuration.

A FUNCTION is not permitted direct memory access.

## Comments

C-style comments are available.

```
VAR
    memloc  :   INT; // declare an INT called memloc
END_VAR
```

Multi-line comments use the `(* ... *)` syntax

```
(* declare
    an INT called
        memloc *) 
    memloc  :   INT;
END_VAR*)
```

## Pragma's

Pragma's are instructions for pre-processing or post-processing of program code.

1. Access instructions READ, READWRITE, NONE in connection with communication to
other hardware platforms
2. Messaging instructions for sending messages to the compilation window during
compilation. Can be TEXT, INFO, WARNING, and ERROR.

Pragma's are not part of the standard and thus manufacturer dependant. In
general pragma's are allowed where a comment would be allowed.

```
{Author: ntphx}
{DI IO 1 = LED stairs}
```

## Standard Data Types

### Numbers

The usual number types are provided by the standard.

|Type     |Bits |Initial Value|
|---------|-----|-------|
|`(U)SINT`|8    |0      |   
|`(U)INT` |16   |0      |   
|`(U)DINT`|32   |0      |   
|`(U)LINT`|64   |0      |   
|`REAL`   |32   |0.0    |   
|`LREAL`  |64   |0.0    |   

A number literal may be expressed in several formats. To increase readability
the use of `_` is allowed.

|Decimal|Binary     |Octal      |Hexadecimal|
|---|---|---|---|
|0       |2#00000000  | 8#000   |16#00 |
|37      |2#0010_0101 | 8#45    |16#25 |
|−14     |−2#00001110 |−8#16    |−16#0E|
|-14     |2#11110010  |8#362    |16#F2 |
|12_534  |2#00110000_11110110   | 8#030366   |16#30f6|

### Bit Strings

Bit Strings are not interpreted as numbers and can be used to store arbitrary
data values. For example status flags, communication data, data from input or
output modules...

|Type   |Bits   |Initial Value|
|---	|---	|---   |
|`BOOL` |1  	|0/FALSE                |
|`BYTE` |8  	|16#00                  |
|`WORD` |16   	|16#0000                |
|`DWORD`|32   	|16#0000_0000           |
|`LWORD`|64   	|16#0000_0000_0000_0000 |

A Bit String literal may be expressed in several formats. To increase readability
the use of `_` is allowed.

### Characters and Text Strings

Encoding for Characters and Text Strings is either ASCII or UTF-16. The encoding
of a literal is determined by the use of different quotation marks.

|Encoding|Literal Format|
|---        |---|
|ASCII          |`'...'` single quotes|
|Unicode UTF-16 |`"abc"` double quotes|

|Type  	   |Bits|Initial Value|Encoding|
|---	   |---|---	|--- |
|`CHAR`    |8       |`'$00'`    |ASCII|
|`WCHAR`   |16      |`"$0000"`  |UTF-16|
|`STRING`  |8/char  |`''`       |ASCII|
|`WSTRING` |16/char |`""`       |UTF-16|

Implementation of Control Characters depends on manufacturer support.

|Character|Control|
|---|---|
|`$L`|Line Feed + Carriage Return   |
|`$N`|New Line                      |
|`$P`|New Page                      |
|`$R`|Return                        |
|`$T`|Tab                           |
|`$$`|Dollar Sign                   |
|`$'`|Single Quotes                 |
|`$"`|Double Quotes                 |

### Date and Time

|Type  	                |Bits   |Initial Value|
|---	                |---	|---	|
|`TIME`   	            |32     |`T#0s`             |
|`LTIME`   	            |64     |`LTIME#0s`         |
|`DATE`   	            |32     |`_`   	            |
|`LDATE`   	            |64     |`LDATE#1970‐01‐01` |
|`TIME_OF_DAY` / `TOD`  |       |`TOD#00:00:00`     |
|`DATE_AND_TIME` / `DT` |       |`_`   	            |

The wider size of `LTIME` and `LDATE` give these types a time precission into
the nanosecond range.

Duration data types will automatically overflow. For example `T#29h41m` will
overflow to `T#1d_5h_41m`.


Time literal examples.

```
    T#25s
    T#‐25s (negative time)
    T#12.4ms
    t#12h
    T#12h23m42s
    t#12h_23m_42s_67ms
    TIME#45m
    time#4m_20s
```

Time and Date literal examples.

```
    DATE#2007‐05‐31
    D#1968‐11‐25
    time_of_day#08:45:00
    TOD#17:30:45
    DATE_AND_TIME#1814‐05‐17‐13:45:00
    dt#2007‐08‐01‐12:30:00
```

### The ANY Type

Functions and operators may work with parameters or operands of differing types.
This ability, called overloading, is indicated in the reference documentation by
use of the `ANY` keyword. Care must be taken when mixing variables of different
types (in any language) as the result will (usually) be cast to the most
restrictive type of parameter or operand.

Some examples of overloaded functions in the standard library

```
    ABS (ANY_NUM)
    SQRT (ANY_REAL)
    
    ADD {+} (ANY_NUM, ANY_NUM)
    MUL {*} (TIME, ANY_NUM)
```

### Enumeration

The enumeration type represents a list of integer values. These values are
constants and so the enumeration cannot be changed once declared.
Declaring an enumeration literal may be done in any accepted numerical format.

```
    evenEnum : (2, 4, 6, 8);
    oddEnum  : (2#0001, 8#0021, 16#05, 7, 2#1001); // decimal (1, 3, 5, 7, 9)
```

The strength of the enumeration lies in the ability to give names to the integer
constants. The values of these constants may than be used by reference to their
names within the enumeration in later code. This makes code easier to
understand and more readable.

When only specifying names in the declaration literal it is assumed the
underlying integers start at zero and increment upwards by one. To avoid this
the values may be specified along with their name.

When referecing an enumeration by variable name only (without specifying a
constant name) by default the first integer value will be returned. To avoid
this the default return value may be specified.

```
    Colors : (Green, Red, Blue);        // integer values (0, 1, 2)
    Card : (Uber := 10, netflix := 16); // integer values (10, 16)
    TimeZones : (PDT, EDT, CET) := EDT  // returns value of EDT by default
    blueColor : Colors := 2; // blueColor is Colors enum with default value Blue (2)
    myCard : Card;           // myCard is Card enum with default value Uber (10)
    (CHANNEL = myCard.Uber); // TRUE if CHANNEL = 10
```

### Arrays

The standard defines arrays with a few limitations

1. Arrays may not have more than 3 dimensions
2. The array bounds must be defined at declaration
3. There is no out-of-bounds checking when accessing the array
4. An array can not be of type `FUNCTION_BLOCK`

#### Declaration 

The bounds of the array must be defined. The indices of these bound need not
start at zero.

```
    1D : ARRAY [2..8] OF USINT;
    2D : ARRAY [0..9] [3..6] OF REAL;
    3D : ARRAY [0..5] [2..8] [4..6] OF BOOL;
```

#### Initialization

By default arrays wil be initialized by the initial value of the data type with which
they were declared. If desired a custom initialization may be done. For very
large arrays a combination of multipliers may be used as seen below. When not
sufficient initial values are specified the remaining array indices will be
initialized with the default initial value of the array data type.

```
    PARTIAL : ARRAY [0..20] of BOOL := [10(1)] // value multiplier
    FULL    : ARRAY [0..4] of BOOL := [TRUE, FALSE, FALSE, TRUE, TRUE];
    MIX : ARRAY [0..9] of REAL := [3.14, 1.60, 4(9.9), 4(0.0)];
    TRIPLE : ARRAY [0..6] of LREAL:= [3(1.1, 2.2)] // [1.1, 2.2,1.1, 2.2, 1.1, 2.2]
```

## User-Defined Data Types

The standard allows for the definition of user-defined data types. Not all basic
data types support user extension. In simple cases a user-defined type is just
an alias for a standard type. Some types, like the structure discussed later, can
only be used when encapsulated by a user-defined type.

A user-defined type must be declared in a `TYPE ... END_TYPE` block. 

```
TYPE
    TrafficLight : (Red, Green, Blue);          // regular enum
    TimeRed : TIME;
    LicensePlates : ARRAY [0..1000] OF WSTRING;
END_TYPE
``` 

### Structures

The STRUCT type is similar its C cousin. It can be used to serve a similar
purpose to the generic object in higher level OOP languages. A field of a
structure may reference another structure 

```
TYPE <struct_name> :
    STRUCT
        <Declaration of datatype 1>;
        <Declaration of datatype 2>;
        ...
        <Declaration of datatype n>
    END_STRUCT
END_TYPE
```

## Variables

Variables are declared in the POU declaration part.

Variable names in IEC 61131 are **not case-sensitive**. The standard defines at
least 6 significant characters for a variable name. This means the first six
characters MUST be unique within the configuration.

TODO: Check if there is implicit casting when assigning a wide literal to a
narrower variable type within the same `ANY` family.

|Variable|Program|Function Block|Function|
|---            |:---:|:---:|:---:|
|`VAR`          |x  |x  |x  |    
|`VAR_TEMP`     |x  |x  |-  |
|`VAR_INPUT`    |x  |x  |x  |
|`VAR_OUTPUT`   |x  |x  |x  |
|`VAR_IN_OUT`   |x  |x  |x  |
|`VAR_EXTERNAL` |x  |x  |-  |
|`VAR_GLOBAL`   |x  |-  |-  |
|`VAR_ACCESS`   |x  |-  |-  |

|Variable|external|internal|Pass-by|
|---            |:---:|:---:|---   |
|`VAR`          |-    |RW   |-     |
|`VAR_TEMP`     |-    |RW   |-     |
|`VAR_INPUT`    |W    |R    |Value |
|`VAR_OUTPUT`   |R    |RW   |Value |
|`VAR_IN_OUT`   |RW   |RW   |Reference|
|`VAR_EXTERNAL` |RW   |RW   |-|
|`VAR_GLOBAL`   |RW   |RW   |-|
|`VAR_ACCESS`   |RW   |RW   |-|

### VAR & VAR_TEMP

The default POU variable `VAR` is local to the POU and may not be accessed by external
POU's. Like all other variables except for `VAR_TEMP` its value is retained in
memory while the PLC is in RUN mode. (Not to be confused with keyword RETAIN
which stores variables in a battery backed memory allowing them to survive a
hard reset).

On the contrary `VAR_TEMP` variables reset their value to the initial value of
their data type after every PLC scan cycle.

### VAR_GLOBAL & VAR_EXTERNAL

`VAR_GLOBAL` variables are defined at the configuration level so they can be made
available to all POU's. In the POU local scope the global variable must be 
declared as `VAR_EXTERNAL` using the exact same name. 

```
CONFIGURATION cfg_1
    VAR_GLOBAL
        ItemCount : UINT;
        AlarmLight : BOOL;
    END_VAR

    PROGRAM POU_1
        VAR_EXTERNAL
            ItemCount : UINT;
            AlarmLight : BOOL;
        END_VAR
    END_PROGRAM
END_CONFIGURATION 
```

### VAR_INPUT, VAR_OUTPUT, and VAR_IN_OUT

These variables function as interface variables between POU's. They're also
called formal parameters. Their access rights are designed to prevent incorrect
access of a POU's data.

In particular access to `VAR_IN_OUT` causes the calling POU to pass data by
reference. This is useful when working with arrays or structures.

```
VAR_INPUT
    parameter1 : INT;
END_VAR

VAR_OUTPUT
    result : INT;
END_VAR

VAR_IN_OUT..END_VAR     // Pass by Reference
```

### RETAIN & CONSTANT

Constant variables have their value defined at compilation and become read-only
thereafter. A variable is made constant using the `CONSTANT` keyword.

```
VAR
    CONSTANT pi : REAL :- 3.14;
END_VAR
```

If data must be able to survive a power interrupt they must be declared with the
`RETAIN` keyword. These variables are stored in PLC memory which is backed by
battery standby power.

```
VAR
    RETAIN Rememberme : DWORD;
    RETAIN Andme AT %IW4 : WORD;
    RETAIN DoorCounter : CTU;   // FB instances can be retained too!
END_VAR
```

### Validation

Variables can have value validitation. Only values within this range can be
assigned to that variable.

```
VAR
    myVAR : DINT (-200..1337)
END_VAR
```

### Casting

Casting is done through use of the built-in functions. General restrictions
apply when casting from a wide to a narrow data type.

```
intValue : INT := REAL_TO_INT(123.10);
```

## Direct Memory Access Method

Direct Memory access is only available to PROGRAM's. To maintain code
compatibility FUNCTION and FUNCTION_BLOCK cannot make assumptions about the
underlying memory structure and physical addresses.

|Prefix 1   |Prefix 2   |Bits   |Type   |
|---|---|---|---|
|`I`|   |   |Input      |
|`Q`|   |   |Output     |
|`M`|   |   |Memory/Flag|
|   |`X`|1  |Boolean    |
|   |`B`|8  |Byte       |
|   |`W`|16 |Word       |
|   |`D`|32 |Double Word|
|   |`L`|64 |Long Word  |

### Memory Access Syntax

```
% <prefix1> <prefix2> <numerical specifiers>
```

Prefix1 and prefix2 determine the general location in memory and the type of
data we want access to. The numerical specifiers determines the exact location
of the data. 

The numerical specifier is usually one or two digits determining the index of
the requests memory location. In some installations the specifier is longer to
include the physical location of the PLC. In this case the order of magnitude is
from left (large) to right (small).

```
%IW10   // Word at index 10 in INPUT memory
%QR5.3  // 3rd BIT of OUTPUT Real at index 5

(* Below the specifier is longer to included physical location data *)
%QW3.4.10   // In factory PLC 3, on module 4, OUTPUT Word at index 10

%MX0.0  Bit 0 (LSB) in memory location 0
%M0.0   SAME
%MB8    Memory byte 8
%MW12   Memory word 12
%MD45   Double word at memory location 45
%ML14   Quadruple word at memory location 14
```

1. Hierachical (direct) adresses
2. Symbolic addresses
3. Normal variables

### Memory Addresses

Since all memory registers are accessible it is possible to overlap the same bit
in memory through different ways of addressing as shown below. Decide beforehand
what areas of memory will serve what purpose and respect the addressing rules
you develop.

![memory_overlap](images/memory_overlap.png)

### Memory Symbols

The term symbol and variable are overlapping.

To keep access to specific memory addresses manageable memory symbols are
introduced. A Memory symbol is simply the naming of a specific address.

```
VAR
    Dig_in AT %IX2.4;
    A_out AT %QW4.1;
END_VAR
```

It's also possible to declare a memory address without specifying a symbol. In
reverse when a symbol is specified without an address the compiler will assign a
free address. The later is the most common scenario.

```
VAR
    AT %M150.1  : BOOL;     // BOOl type declared at memory address 150 bit 1
    number      : UNT;      // 32 bit address space declared by compiler
END_VAR
```

### Unspecified I/O address

It's possible to use generic addresses for IO modules in a PROGRAM which must be
implemented through a form of inheritance once the code is deployed in the
field.

Here PROGRAM `GenericModule` specifies two generic input and output memory
addresses. These are later defined in the actual field implementation through 
`VAR_CONFIG` in a `GLOBAL VARIABLE LIST`

```
PROGRAM GenericModule
    input   AT  %I.*    : DWORD;
    output  AT  %O.*    : BOOL;

    VAR_CONFIG
        GenericModule.input  AT %I2.3    : DWORD;
        GenericModule.output AT %O1.0    : BOOL;
    END_VAR
END_PROGRAM
```

## Operators

### Operator Precedence

|Rank|Symbol|Comment|
|---|---            |---                        |
|1  |`( )`          |Parenthesis                |
|2  |`function()`   |Function calls             |
|3  |`-` , `NOT`    |Negation and Complement    |
|4  |`*, /, MOD`    |Multiplication and Division|
|5  |`+, -`         |Sums                       |
|6  |`<, >, <=, >=` |Comparison                 |
|7  |`=, <>`        |Equality & Inequality      |
|8  |`&` , `AND`    |Binary AND                 |
|9  |`XOR`          |Binary XOR                 |
|10 |`OR`           |Binary OR                  |

### Assignment

Assignment is from right to left 

```
OP1 := OP2  // value of OP2 is assigned to OP1
```

TODO

In C an assignment also returns the value of that assignment so it can be used
in a loop. Test if this also happens with ST.

```
WHILE (OP1 := OP2) DO
    (* In C this would loop until OP2 is 0 *)
END_WHILE
```

### Comparison

|Symbol  |Function Call|
|---|---|
|`>` |`GT`|
|`>=`|`GE`|
|`=` |`EQ`|
|`<` |`LT`|
|`<=`|`LE`|
|`<>`|`NE`|

## Overflow

Overflow seems to be definite behaviour and results in the value looping around
to the other negative or positive maximum to continue from there

## Control

### IF Selection

Every `IF` or `ELSIF` statement must be followed by an `END_IF`. The boolean
expression may be any expression or statement which returns a BOOL value or any
value which can be interpreted as such.

```
IF <boolean expression> THEN
    ELSIF <boolean expression> THEN
        ELSE
        // statements
    END_IF
END_IF
```

### CASE Selection

The `CASE` statement works with variables of type `ANY_INT` and Enumurations.
`CASE` supports a single value per line or multiple values per line separated by
comma's. It's also possible to specify a range of values using `..`

TODO: Determine if `CASE` also displays fallthrough behavior as in Python. There
seems to be no `BREAK` statement required.

```
(* Converts an angle in degrees to radians*)

PROGRAM Degrees_To_Radian
VAR
    Degrees : USINT;
    Radians : REAL;
    CONSTANT Pi : REAL := 3.14;
    Error   : BOOL := FALSE;
END_VAR

CASE Degrees OF
    0:
        Radians := 0.0;
    1..179, 181..359:
        Radians := Pi / 180 * Degrees;
    180:
        Radians := Pi;
    360:
        Radians := 2 * Pi; 
    ELSE:
        Error := TRUE; 
END_CASE
END_PROGRAM
```

Enumerations allow you to name a series of constants. If these constants are of
`ANY_INT` type they can be used in `CASE` for improved readability.    

```
PROGRAM CAR_LIGHTS
VAR
    Intensity: (Off:=0, Low:=1, Medium:=2, High:=3); //Enumeration
    Signal: UINT;
END_VAR
    
CASE Signal OF
    Intensity.Off:
        Signal := 0;
    Intesity.Low, Intensity.Medium:
        Signal := 10;
    Intensity.High:
        Signal := 99;
END_CASE
END_PROGRAM
```

## Loops

The conditional expression in some loops may consist of multiple boolean
expressions. Only the end result of that combination is used to determine loop
execution.

### FOR Loop

The Index variable in a FOR loop is automatically incremented by 1. If a
different increment is required it must be specified.

```
FOR Index := 1 TO 10 DO
    // Loop will execute 10 times
END_FOR

FOR Index := 2 TO 20 BY 2 DO
    // Also executes 10 times
END_FOR
```

### WHILE Loop

This loop is meant to execute `WHILE` a given expression evaluates TRUE. It may
not execute at all.

```
WHILE ( expr1 AND expr2 ) OR expr3 DO
    // statements
END_WHILE
```

### REPEAT Loop

This loop is meant to `REPEAT` until a given expression evaluates TRUE. It is
guaranteed at least one execution.

```
REPEAT
    // statements
UNTIL var > 100;
```

### EXIT Statement

`EXIT` breaks out of the loop it is placed in. Outer loops may continue.

## Functions

Functions may have any number of parameters and a single return value. The
return value is stored in a variable with the same name as the function. The
return type must be specified in the function header.

```
FUNCTION isEvenNumber : BOOL        // specify return type here
    VAR_INPUT
        aNumber : LINT;
    END_VAR
    VAR
        Two : USINT := 2;
    END_VAR
    
    IF ( aNumber MOD Two ) = 0 THEN
        isEvenNumber := TRUE;
    ELSE
        isevenNumber := FALSE;
    END_IF
END_FUNCTION                        // not required in CODESYS
```

### Standard Functions

#### Mathematics

|Function   |Description|
|---        |---    |
|`ABS(ANY_NUM)`     |Absolute value of ANY_NUM|
|`SQRT(ANY_REAL)`   |Square Root of positive ANY_REAL|
|`LN()`             |Natural Log (e)|
|`LOG()`            |Base-10 Log|
|`EXP(X)`           |Natural exponent e^X|
|`SIN(rad)`         |Sine of rad|
|`ASIN()`           |Arcsine of rad|
|`COS(rad)`         |Cosine of rad|
|`ACOS()`           |Arccosine of rad|
|`TAN(rad)`         |Tangent of rad|
|`ATAN()`           |ArcTangent of rad|

#### Selection

|Function   |Description|
|---        |---    |
|`SEL(INDEX, EL1, EL2)`      |Return EL1 or EL2 based on INDEX|
|`MUX(INDEX, EL1, .., ELn)`  |Return element at INDEX|
|`MIN(INDEX, EL1, .., ELn)`  |Return lowest value|
|`MAX(INDEX, EL1, .., ELn)`  |Return highest|
|`LIMIT(IN, valMIN, valMAX)` |Returns valMin or valMax if IN is outside|


#### Bit string functions

|Function   |Description|
|---        |---    |
|`SHL(BIT_STRING. n)`|Shifts bits n places left, adds zeroes from right|
|`SHR(BIT_STRING, n)`|Shifts bits n places right, adds zeroes from left|
|`ROL(BIT_STRING, n)`|Shifts bits n places left. Bits falling off left are added on the right|
|`ROR(BIT_STRING, n)`|Shifts bits n places right. Bits falling off right are added on the left|

#### Text string functions

|Function   |Description| 
|---        |---    |
|`LEN()`                    |Get length of string|
|`LEFT(STRING, N)`          |Get N chars of string from left|
|`RIGHT(STRING, N)`         |Get N chars of string from right|
|`MID(STRING, N, M)`        |Get N chars from string index M count to left|
|`CONCAT(aStr, bStr)`       |Concat 2 strings|
|`INSERT(aStr, bStr, N)`    |Insert bStr into aStr at index N|
|`DELETE(STRING, N, M)`     |Deletes N chars in STRING from index M to the left|
|`REPLACE(aStr, bStr, N, M)`|Replace N chars in aStr with N chars from bStr at aStr index M to the left|
|`FIND(aStr, bStr)`         |Search aStr for substring bStr and return index or zero|

```
LEN("Hi World");                    // 8
LEFT("New York City, 7);            // "New Yor"
RIGHT("Chicago", 3);                // "ago"
MID("Johny Bravo", 7, 5);           // "hny B"
CONCAT("London", "Bridge";)         // "LondonBridge"
INSERT("Delicious", "Delhi", 4);    // "DeliDelhicious
DELETE("Auckland NZ", 3, 6);        // "Aucnd NZ"
REPLACE("Swing", "Rope", 2, 2);     // "SRong"
FIND("Whitehouse", "house");        // 5 (would be 0 if "house" not found)
```

## Standard Function Blocks

The Standard defines the following types of Function Blocks.

### Flank/Edge Detection

![flank](images/flank.png)

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT  |CLK|`BOOL`|Bool variable on which flank detection is performed|
|OUTPUT |Q  |`BOOL`|Remains TRUE until next scan cycle|

Flank detection determines if variable CLK is changing **during** the current
scan cycle. Output variable Q will only remain TRUE during the
scan cycle within which the flank was detected. If the input has not changed in
the next scan cycle the ouput variable Q will be FALSE.

### Bistable Relays (Teleruptors)

![teleruptor](images/teleruptor.png)

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT|Set|`BOOL`|Dominant in an SR teleruptor|
|INPUT|Reset|`BOOL`|Dominant in an RS teleruptor|
|OUTPUT|Q|`BOOL`||

Output Q is switched high when input Set is high. Outut Q then remains high even
when Set is switched low. Q can only be reset to low switching input Reset to
high. This means the bistable relay's output value can survive a cold restart
like a power failure.

Dominant determines what should happens to Q if both Set and Reset become TRUE
at the same time. The dominant Input determines the result here.

### Timers

![timer](images/timer.png)

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT|IN|`BOOL`|Rising edge (TON) or Falling edge (TOF) starts timer|
|INPUT|PT|`TIME`|Desired Time delay|
|OUTPUT|Q|`BOOL`|Output depends on timer mode.
|OUTPUT|ET|`TIME`|Is the Time elapsed since IN came logically high|

Types of timers:

1. **TON** when IN goes high and after a specified time delay of PT output Q
   will be switched high. IN must be high for a minimum time.
2. **TOF** when IN goes high output Q is immediately switched high and remains
   high for specified time PT. If IN is reset and switches to high again TON
   will restart for time PT.
3. **TP** when IN goes high output Q is switched high but further resets of IN
   will not affect Q. It will not restart.

### Counters

Types of counters:

1. **CTU** counts upwards
2. **CTD** counts downwards
3. **CTUD** can count both upwards and downwards

![ctu](images/ctu.png)

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT  |CU|`BOOL`   |Count the rising edges of this input|
|INPUT  |PV|`ANY_INT`|The limit of this counter|
|INPUT  |R |`BOOL`   |When R=TRUE then CV=0|
|OUTPUT |CV|`ANY_INT`|Current value of the counter|
|OUTPUT |Q |`BOOL`   |When PV=CV then Q=TRUE|

![ctd](images/ctd.png)

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT  |CD|`BOOL`   |Count the rising edges of this input|
|INPUT  |PV|`ANY_INT`|The limit of this counter|
|INPUT  |LD|`BOOL`   |When LD=TRUE then CV=PV|
|OUTPUT |CV|`ANY_INT`|Current value of the counter|
|OUTPUT |Q |`BOOL`   |When CV=0 then Q=TRUE|

![ctud](images/ctud.png)

CTUD can count both ways - NB must check the function of R and LD in this
particular case. Resetting them according to the same logic as CTD or CTU would
result in at least QU or QD being TRUE on any reset.

|Kind|Field|Type|Description|
|---|---|---|---|
|INPUT  |CU|`BOOL`   |Count the rising edges of this input|
|INPUT  |CD|`BOOL`   |Count the rising edges of this input|
|INPUT  |PV|`ANY_INT`|The target of this counter|
|INPUT  |R |`BOOL`   |When R=TRUE then CV=0|
|INPUT  |LD|`BOOL`   |When LD=TRUE then CV=PV|
|OUTPUT |CV|`ANY_INT`|Current value of the counter|
|OUTPUT |QU|`BOOL`   |When PV=CV then QU=TRUE|
|OUTPUT |QD|`BOOL`   |When CV=0 then QD=TRUE|

### User-defined Function Blocks

Declaration is similar to a regular function except there is no return value.

```
(* Example function block tracks the number of cycles a door goes through. Once
it reaches a limit an alarm must be triggered for an inspection *)

FUNCTION_BLOCK DOOR_CYCLE_ALARM
VAR_INPUT
    CYCLE : BOOL;
    ALARM_LENGTH : TIME := T#15s;   // Timelength the alarm should sound
END_VAR
VAR RETAIN
    TOTAL : SINT := 0;
END_VAR
VAR_OUTPUT
    ALARM : BOOL := FALSE;
END_VAR
VAR
    TRESHOLD : SINT := 1000;
END_VAR

TOTAL := TOTAL + CYCLE;

IF (TOTAL > TRESHOLD) THEN
    ALARM := TRUE
END_IF

END_FUNCTION_BLOCK
```

A FB may call another FB if required. Here an instance of TP (Timer) is used by
a FB representing a traffic light.

```
(* Previously declared enumeration Colors := (Green, Yellow, Red) 
Here Green = 1, Yellow = 2, and Red = 3 *)

FUNCTION_BLOCK  TrafficLight
VAR_INPUT
    RUN : BOOL := FALSE;        // Starts or Stops the light
    DURATION : TIME := T#20s    // Time between light changes - defaults to 20s
END_VAR
VAR_OUTPUT 
    STATUS : USINT := Colors.Green; // Default light color is green
END_VAR
VAR
    TIMER : TP;                 // Timer FB instance
END_VAR

IF NOT TIMER.Q THEN
    TIMER(IN := RUN, PT := DURATION);
    
    CASE STATUS OF
        Colors.Green, Colors.Yellow:
            STATUS := STATUS + 1;
        Colors.Red:
            STATUS := Colors.Green  // Loop back to green from red
    END_CASE
END_IF

```

### Using a Function Block instance

The syntax for calling a FB instance is similar to Python's keywords. Note how
not all input variables need to be specified in the call. If an input variable
is not specified it is assigned its initial value in accordance with the
standard.

TODO

Verify this happens on every call or if previous call's input values are
retained.

```
fb_door_cycle_alarm(CYCLE := 1, ALARM_LENGTH := T#25s);
```

Access to output variables is similar to the OOP `object.field` approach.

```
IF (fb_door_cycle_alarm.ALARM) THEN
    // code
END_IF
```

It's possible to assign output variable values directly at the FB call using the
`=>` operator.

```
VAR
    RINGER : BOOL := FALSE;
END_VAR

fb_door_cycle_alarm(CYCLE := 1, ALARM_LENGTH := T#25s, ALARM => RINGER);
```

## APPENDICES

### Two's complement

Numbers can be either signed or unsigned. To represent negativelly signed
counterpart of a positive number the most significant bit is set to `1`. Next
all other bits are flipped and decimal `1` is added to that result. The addition
of a `1` at the end is needed because positive and negative numbers with the
same absolute value are off-set by 1 in binary since the absolute value 0 has no
negative counterpart.

|Decimal|8-Bit Binary|Decimal Addition|
|---    |---   |--- |
|`+15`   |`0000_1111`|`0 + 0 + 0 + 0 + 8 + 4 + 2 + 1`|
|`-15`   |`1111_0001`|`-128 + 64 + 32 + 16 + 0 + 0 + 0 + 1`|

### Endianness

![endian](images/endian.png)

Big Endian with most significant bit indicating sign


